package de.kfindeisen.intellijplugins.awesomecaretlocator.ide.caret

import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import junit.framework.TestCase
import java.awt.datatransfer.DataFlavor

class CaretInFileCopierTest : BasePlatformTestCase() {

	override fun getTestDataPath() = "src/test/resources/testfiles/"

	fun `test caret before java class`() {
		doTest("javaclasses/Main1.java", "src/javaclasses/Main1.java:1:1")
	}

	fun `test caret in java class`() {
		doTest("javaclasses/Main2.java", "src/javaclasses/Main2.java:5:5")
	}

	fun `test caret after java class`() {
		doTest("javaclasses/Main3.java", "src/javaclasses/Main3.java:7:2")
	}

	fun `test caret before java interface`() {
		doTest("javainterfaces/Main1.java", "src/javainterfaces/Main1.java:1:1")
	}

	fun `test caret in java interface`() {
		doTest("javainterfaces/Main2.java", "src/javainterfaces/Main2.java:4:35")
	}

	fun `test caret after java interface`() {
		doTest("javainterfaces/Main3.java", "src/javainterfaces/Main3.java:5:2")
	}

	fun `test caret before kotlin class`() {
		doTest("kotlinclasses/Main1.kt", "src/kotlinclasses/Main1.kt:1:1")
	}

	fun `test caret in kotlin class`() {
		doTest("kotlinclasses/Main2.kt", "src/kotlinclasses/Main2.kt:5:9")
	}

	fun `test caret after kotlin class`() {
		doTest("kotlinclasses/Main3.kt", "src/kotlinclasses/Main3.kt:7:2")
	}

	fun `test caret at start of text file`() {
		doTest("textfiles/lorem1.txt", "src/textfiles/lorem1.txt:1:1")
	}

	fun `test caret in middle of text file`() {
		doTest("textfiles/lorem2.txt", "src/textfiles/lorem2.txt:3:5")
	}

	fun `test caret at end of text file`() {
		doTest("textfiles/lorem3.txt", "src/textfiles/lorem3.txt:6:6")
	}

	private fun doTest(testFile: String, expectation: String? = null, shouldBeVisible: Boolean = expectation != null) {
		myFixture.configureByFile(testFile)

		val action = CaretInFileCopier()

		val presentation = myFixture.testAction(action)
		TestCase.assertEquals("The visibility of the copy action is not as expected.",
			shouldBeVisible, presentation.isEnabledAndVisible)

		if (!presentation.isEnabledAndVisible) {
			return
		}

		val actionId = action::class.qualifiedName ?: throw AssertionError("Unable to find copy action.")

		myFixture.performEditorAction(actionId)
		val actual = CopyPasteManager.getInstance().getContents<String>(DataFlavor.stringFlavor)
		TestCase.assertEquals("The copied caret location is not as expected.",
			expectation, actual)
	}
}
