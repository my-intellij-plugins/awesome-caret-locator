package de.kfindeisen.intellijplugins.awesomecaretlocator.ide.caret

import com.intellij.openapi.actionSystem.ex.ActionUtil
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.testFramework.EditorTestUtil
import com.intellij.testFramework.LightPlatformCodeInsightTestCase
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import junit.framework.TestCase
import java.awt.datatransfer.DataFlavor

class CaretInClassCopierTest : BasePlatformTestCase() {

	override fun getTestDataPath() = "src/test/resources/testfiles/"

	fun `test caret before java class`() {
		doTest("javaclasses/Main1.java")
	}

	fun `test caret in java class`() {
		doTest("javaclasses/Main2.java", "testfiles.javaclasses.Main:5:5")
	}

	fun `test caret after java class`() {
		doTest("javaclasses/Main3.java")
	}

	fun `test caret before java interface`() {
		doTest("javainterfaces/Main1.java")
	}

	fun `test caret in java interface`() {
		doTest("javainterfaces/Main2.java", "testfiles.javainterfaces.Main:4:35")
	}

	fun `test caret after java interface`() {
		doTest("javainterfaces/Main3.java")
	}

	fun `test caret before kotlin class`() {
		doTest("kotlinclasses/Main1.kt")
	}

	fun `test caret in kotlin class`() {
		doTest("kotlinclasses/Main2.kt", "testfiles.kotlinclasses.Main:5:9")
	}

	fun `test caret after kotlin class`() {
		doTest("kotlinclasses/Main3.kt")
	}

	fun `test caret at start of text file`() {
		doTest("textfiles/lorem1.txt")
	}

	fun `test caret in middle of text file`() {
		doTest("textfiles/lorem2.txt")
	}

	fun `test caret at end of text file`() {
		doTest("textfiles/lorem3.txt")
	}

	private fun doTest(testFile: String, expectation: String? = null, shouldBeVisible: Boolean = expectation != null) {
		myFixture.configureByFile(testFile)

		val action = CaretInClassCopier()

		val presentation = myFixture.testAction(action)
		TestCase.assertEquals("The visibility of the copy action is not as expected.",
			shouldBeVisible, presentation.isEnabledAndVisible)

		if (!presentation.isEnabledAndVisible) {
			return
		}

		val actionId = action::class.qualifiedName ?: throw AssertionError("Unable to find copy action.")

		myFixture.performEditorAction(actionId)
		val actual = CopyPasteManager.getInstance().getContents<String>(DataFlavor.stringFlavor)
		TestCase.assertEquals("The copied caret location is not as expected.",
			expectation, actual)
	}
}
