package de.kfindeisen.intellijplugins.awesomenumbersearcher.ide.localization

import com.intellij.DynamicBundle
import org.jetbrains.annotations.Nls
import org.jetbrains.annotations.PropertyKey

private const val BUNDLE = "localization.Bundle"

object ResourceBundle : DynamicBundle(BUNDLE) {

	@JvmStatic
	@Nls
	fun message(@PropertyKey(resourceBundle = BUNDLE) key: String, vararg params: Any): String = messageOrDefault(key, "", *params)
}