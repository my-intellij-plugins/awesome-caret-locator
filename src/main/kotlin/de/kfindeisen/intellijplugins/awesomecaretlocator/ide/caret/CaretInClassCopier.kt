package de.kfindeisen.intellijplugins.awesomecaretlocator.ide.caret

import com.intellij.ide.actions.FqnUtil
import com.intellij.lang.java.JavaLanguage
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.openapi.vfs.findPsiFile
import com.intellij.psi.PsiElement
import de.kfindeisen.intellijplugins.awesomecaretlocator.ide.icons.Icons
import de.kfindeisen.intellijplugins.awesomenumbersearcher.ide.localization.ResourceBundle
import org.jetbrains.kotlin.idea.KotlinLanguage
import org.jetbrains.uast.UClass
import org.jetbrains.uast.toUElementOfType
import java.awt.datatransfer.StringSelection

class CaretInClassCopier : AnAction(
	ResourceBundle.message("copy.in.class.action.text"),
	ResourceBundle.message("copy.in.class.action.description"),
	Icons.CARET
) {

	override fun update(e: AnActionEvent) {

		val project = e.getData(CommonDataKeys.PROJECT) ?: run {
			e.presentation.isEnabledAndVisible = false
			return
		}

		val editor = e.getData(CommonDataKeys.EDITOR) ?: run {
			e.presentation.isEnabledAndVisible = false
			return
		}

		val currentPsiFile = editor.virtualFile.findPsiFile(project) ?: run {
			e.presentation.isEnabledAndVisible = false
			return
		}

		val language = currentPsiFile.language

		if (language !is JavaLanguage && language !is KotlinLanguage) {
			e.presentation.isEnabledAndVisible = false
			return
		}

		val element = currentPsiFile.findElementAt(editor.caretModel.offset) ?: run {
			e.presentation.isEnabledAndVisible = false
			return
		}

		e.presentation.isEnabledAndVisible = findClassElement(element) != null
	}

	override fun actionPerformed(e: AnActionEvent) {
		val project = e.getData(CommonDataKeys.PROJECT) ?: return

		val editor = e.getData(CommonDataKeys.EDITOR) ?: return

		val currentPsiFile = editor.virtualFile.findPsiFile(project) ?: return

		val position = editor.caretModel.logicalPosition
		val line = position.line + 1
		val col = position.column + 1

		val element = currentPsiFile.findElementAt(editor.caretModel.offset) ?: return

		val classElement = findClassElement(element) ?: return
		val fullyQualifiedName = FqnUtil.elementToFqn(classElement, editor)
		val classLocation = "$fullyQualifiedName:$line:$col"

		CopyPasteManager.getInstance().setContents(StringSelection(classLocation))
	}

	private fun findClassElement(element: PsiElement): PsiElement? {
		var classElement = element
		var count = 0
		while (classElement.toUElementOfType<UClass>() == null && count < 50) {
			classElement = classElement.parent ?: return null
			count++
		}
		if (count >= 50) {
			return null
		}
		return classElement
	}
}
