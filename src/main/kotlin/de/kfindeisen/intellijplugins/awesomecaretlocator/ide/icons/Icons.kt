package de.kfindeisen.intellijplugins.awesomecaretlocator.ide.icons

import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object Icons {

	val CARET = load("/icons/caret.svg")

	private fun load(path: String): Icon = IconLoader.getIcon(path, Icons::class.java)
}