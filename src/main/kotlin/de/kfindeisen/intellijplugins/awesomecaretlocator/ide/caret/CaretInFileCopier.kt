package de.kfindeisen.intellijplugins.awesomecaretlocator.ide.caret

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.PlatformCoreDataKeys
import com.intellij.openapi.ide.CopyPasteManager
import de.kfindeisen.intellijplugins.awesomecaretlocator.ide.icons.Icons
import de.kfindeisen.intellijplugins.awesomenumbersearcher.ide.localization.ResourceBundle
import java.awt.datatransfer.StringSelection

class CaretInFileCopier : AnAction(
	ResourceBundle.message("copy.in.file.action.text"),
	ResourceBundle.message("copy.in.file.action.description"),
	Icons.CARET
) {

	override fun actionPerformed(e: AnActionEvent) {
		val editor = e.getData(CommonDataKeys.EDITOR) ?: return

		val position = editor.caretModel.logicalPosition
		val line = position.line + 1
		val col = position.column + 1

		val projectFile = e.getData(PlatformCoreDataKeys.PROJECT_FILE_DIRECTORY)?.path ?: return
		val currentFile = editor.virtualFile.path
		val path = currentFile.removePrefix(projectFile).removePrefix("/")
		val fileLocation = "$path:$line:$col"

		CopyPasteManager.getInstance().setContents(StringSelection(fileLocation))
	}
}
