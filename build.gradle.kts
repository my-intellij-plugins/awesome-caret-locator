import java.net.URI

plugins {
	id("java")
	id("org.jetbrains.kotlin.jvm") version "1.9.21"
	id("org.jetbrains.intellij") version "1.16.1"
}

group = prop("pluginGroup")
version = prop("pluginVersion")

repositories {
	mavenCentral()
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
	version.set(prop("platformVersion"))
	type.set(prop("platformType"))

	plugins.set(listOf("com.intellij.java", "org.jetbrains.kotlin"))
}

tasks {
	prop("javaVersion").let {
		// Set the JVM compatibility versions
		withType<JavaCompile> {
			sourceCompatibility = it
			targetCompatibility = it
		}
		withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
			kotlinOptions.jvmTarget = it
		}
	}

	patchPluginXml {
		version.set(prop("pluginVersion"))

		sinceBuild.set(prop("platformSinceBuild"))
		untilBuild.set(prop("platformUntilBuild"))

		changeNotes.set(file("src/main/resources/META-INF/change-notes.html").readText())
		pluginDescription.set(file("src/main/resources/META-INF/description.html").readText())
	}

	signPlugin {
		certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
		privateKey.set(System.getenv("PRIVATE_KEY"))
		password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
	}

	publishPlugin {
		token.set(System.getenv("PUBLISH_TOKEN"))
	}
}

fun prop(name: String)
		= extra.properties[name] as? String
	?: error("Property `$name` is not defined in gradle.properties")
